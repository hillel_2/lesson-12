from django.http import HttpResponse
from django.shortcuts import render

from market.models import Wallpaper


def list(request):
    wallpapers = Wallpaper.objects.all()
    return render(request,
                  'market/wallpapers.html',
                  context={'wallpapers': wallpapers})


def item(request, item_id):
    wallpaper = Wallpaper.objects.get(pk=item_id)
    return render(request,
                  'market/wallpaper.html',
                  context={'wallpaper': wallpaper})