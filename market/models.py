from django.db import models


class Brand(models.Model):
    name = models.CharField(max_length=50)
    rating = models.FloatField(max_length=5)

    def __str__(self):
        return f'{self.id}-{self.name}({self.rating})'


class Wallpaper(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    price = models.FloatField(null=True, blank=True)
    available = models.BooleanField(default=False)
    quantity = models.IntegerField(default=0)
    description = models.TextField()

    def __str__(self):
        return f'{self.brand} - {self.name}'