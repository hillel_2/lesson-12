from django.contrib import admin

from market.models import Wallpaper, Brand

admin.site.register(Brand)
admin.site.register(Wallpaper)